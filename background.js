const SCORE = "69.42";

const API_URL =
  "https://europe-west1-montblanc-race.cloudfunctions.net/register";

console.log("extension 1.0 to overwrite montblanc score is ready: ", SCORE);

async function overwriteScore(e) {
  if (e.method === "POST") {
    console.log("intercepting POST request to ", e.url);

    const bs = new Uint8Array(e.requestBody.raw[0].bytes);
    const postedString = decodeURIComponent(
      String.fromCharCode.apply(null, bs)
    );
    console.log("initial request body: ", postedString);

    const bod = JSON.parse(postedString);

    if (bod.score === SCORE) {
      console.log("score already set, aborting");
      return;
    }

    bod.score = SCORE;
    console.log(bod);

    const resp = await fetch(API_URL, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: "Bearer VgDa2q5ZPfcyA+Fal9CtlGXqOb+T",
      },
      body: JSON.stringify(bod),
    });
    console.log(resp);

    // cancel initial request
    return { cancel: true };
  }
}

browser.webRequest.onBeforeRequest.addListener(
  overwriteScore,
  { urls: [API_URL] },
  ["blocking", "requestBody"]
);
